params=$1
if [ "X${params}" != "X" ]; then
	echo "Running Puppet with --debug"
	sudo puppet apply /etc/puppet/code/environments/master/site.pp --debug
else
	sudo puppet apply /etc/puppet/code/environments/master/site.pp 
fi
