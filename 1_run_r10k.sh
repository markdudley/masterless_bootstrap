arg=$1

function usage {
	echo "Usage: run_r10k.sh <environment>"
        exit 1
}

if [ "X${arg}" == "X-h" ]; then
	usage
elif [ "X${arg}" == "X" ]; then
	usage
else
	env=$arg
	echo "Running r10k on this environment: $env"
fi
sudo ./setup/bundlebin/r10k deploy environment $env --puppetfile --verbose
